# Python Plugin Registry
## Introduction

A python plugin manager which provides:

* A tree based structure for plugin registry and organization Automatic plugin discovery
* via setuptools entry points import plugins using the registry path as namespace
* modules

## Current State and Development Plans

This library is being developed for the [Moon
Nectar](https://gitlab.com/selcouth/moon-nectar) project and will release a stable
version (1.0.0) along with that project.  While all releases should be usable, well
tested and stable, backwards compatibility will not be a consideration until after
version 1.0.0 and documentation may be missing or incomplete.

## Contributing

Bug reports and merge requests are always welcomed. Coding style for this library is
strict [Python Black](https://github.com/python/black).  
