import pkg_resources
import pytest
import re

from pregistry import Registry as Registry_
from pregistry import Plugin

class Registry(Registry_, base=True):
    _entry_point_group = "pregistry.test"


class Comp1(Registry, parent=Registry): ...
class Comp2(Registry, parent=Registry): ...
class Comp2a(Registry, parent=Comp2): ...
class Comp2b(Registry, parent=Comp2): ...
class Comp2a1(Registry, parent=Comp2a): ...

class PluginComp1(Plugin, registry=Comp1): ...
class PluginComp2(Plugin, registry=Comp2): ...
class PluginComp2a(Plugin, registry=Comp2a): ...
class PluginComp2b(Plugin, registry=Comp2b): ...
class PluginComp2a1(Plugin, registry=Comp2a1): ...
class PluginComp2a1Sub(PluginComp2a1, registry=Comp2a1): ...

child_tests = [
        (Registry, ['Comp1',  'Comp2'],  []),
        (Comp1,    [],                   ['PluginComp1']),
        (Comp2,    ['Comp2a', 'Comp2b'], ['PluginComp2']),
        (Comp2a,   ['Comp2a1'],          ['PluginComp2a']),
        (Comp2b,   [],                   ['PluginComp2b']),
        (Comp2a1,  [],                   ['PluginComp2a1','PluginComp2a1Sub']),
        ]

registry_parent_name_tests = [
        (Registry, None,     True,  ),
        (Comp1,    Registry, False, ),
        (Comp2,    Registry, False, ),
        (Comp2a,   Comp2,    False, ),
        (Comp2b,   Comp2,    False, ),
        (Comp2a1,  Comp2a,   False, ),
        ]

plugin_parent_tests = [
        (PluginComp1,      Comp1),
        (PluginComp2,      Comp2),
        (PluginComp2a,     Comp2a),
        (PluginComp2b,     Comp2b),
        (PluginComp2a1,    Comp2a1),
        (PluginComp2a1Sub, Comp2a1),
        ]

path_tests_string = [
        (Registry,         'Registry', ),
        (Plugin,           'Plugin', ),
        (Comp1,            'Registry.Comp1', ),
        (Comp2,            'Registry.Comp2', ),
        (Comp2a,           'Registry.Comp2.Comp2a', ),
        (Comp2b,           'Registry.Comp2.Comp2b', ),
        (Comp2a1,          'Registry.Comp2.Comp2a.Comp2a1', ),
        (PluginComp1,      'Registry.Comp1.PluginComp1', ),
        (PluginComp2,      'Registry.Comp2.PluginComp2', ),
        (PluginComp2a,     'Registry.Comp2.Comp2a.PluginComp2a', ),
        (PluginComp2b,     'Registry.Comp2.Comp2b.PluginComp2b', ),
        (PluginComp2a1,    'Registry.Comp2.Comp2a.Comp2a1.PluginComp2a1', ),
        (PluginComp2a1Sub, 'Registry.Comp2.Comp2a.Comp2a1.PluginComp2a1Sub', ),
        ]

path_tests_navigation = [
        (Comp1,            'Registry["Comp1"]',                                          'Registry.Comp1'),
        (Comp1,            'Registry()["Comp1"]',                                        'Registry.Comp1'),
        (Comp1,            'Registry()["Comp1"]',                                        'Registry().Comp1'),
        (Comp2,            'Registry["Comp2"]',                                          'Registry.Comp2'),
        (Comp2a,           'Registry["Comp2"]["Comp2a"]',                                'Registry.Comp2.Comp2a'),
        (Comp2b,           'Registry["Comp2"]["Comp2b"]',                                'Registry.Comp2.Comp2b'),
        (Comp2a1,          'Registry["Comp2"]["Comp2a"]["Comp2a1"]',                     'Registry.Comp2.Comp2a.Comp2a1'),
        (PluginComp1,      'Registry["Comp1"]["PluginComp1"]',                           'Registry.Comp1.PluginComp1'),
        (PluginComp2,      'Registry["Comp2"]["PluginComp2"]',                           'Registry.Comp2.PluginComp2'),
        (PluginComp2a,     'Registry["Comp2"]["Comp2a"]["PluginComp2a"]',                'Registry.Comp2.Comp2a.PluginComp2a'),
        (PluginComp2b,     'Registry["Comp2"]["Comp2b"]["PluginComp2b"]',                'Registry.Comp2.Comp2b.PluginComp2b'),
        (PluginComp2a1,    'Registry["Comp2"]["Comp2a"]["Comp2a1"]["PluginComp2a1"]',    'Registry.Comp2.Comp2a.Comp2a1.PluginComp2a1'),
        (PluginComp2a1Sub, 'Registry["Comp2"]["Comp2a"]["Comp2a1"]["PluginComp2a1Sub"]', 'Registry.Comp2.Comp2a.Comp2a1.PluginComp2a1Sub'),
        ]


class TestRegistry:

    @pytest.mark.parametrize("cls,parent, base", registry_parent_name_tests)
    def test_registry_parent_names(self, cls, parent, base):
        assert cls.registry == parent
        assert cls._base == base
        if not base:
            assert cls._name in parent

    @pytest.mark.parametrize("cls,parent", plugin_parent_tests)
    def test_plugin_parent(self, cls, parent):
        assert cls.registry == parent
        assert cls.__name__ in parent

    @pytest.mark.parametrize("cls,path", path_tests_string)
    def test_registry_paths(self, cls, path):
        assert cls.path == path

    @pytest.mark.parametrize("cls,path", path_tests_string)
    def test_registry_get(self, cls, path):
        if not cls == Plugin:
            assert Registry.get(path) == cls
            assert Registry.get(path.split('.')) == cls

    def test_registry_get_missing(self):
        assert Registry.get('invalid') is None
        assert Registry.get('invalid', default='missing') == 'missing'

    @pytest.mark.parametrize("cls,askey,asattr", path_tests_navigation)
    def test_registry_navigate(self, cls, askey, asattr):
        assert eval(askey)  is cls
        assert eval(asattr) is cls

    @pytest.mark.parametrize("cls,registries,plugins", child_tests)
    def test_registry_children(self, cls, registries, plugins):
        for reg in registries:
            assert reg in dir(cls)
            assert reg in [_._name for _ in cls.registries]

        for plugin in plugins:
            assert plugin in dir(cls)
            assert plugin in [_.__name__ for _ in cls.plugins]

    def test_registry_invalid(self):
        with pytest.raises(KeyError):
            Registry["does_not_exist"]
        with pytest.raises(AttributeError):
            Registry.does_not_exist

    @pytest.mark.parametrize("cls,path", path_tests_string)
    def test_plugin_entry_points(self, cls, path):
        if isinstance(cls, Plugin) and cls.registry is not None:
            assert cls.entry_point == f"{path} = test_registry:{cls.__name__}"
        elif cls.registry is None and issubclass(cls, Plugin):
            assert cls.entry_point is None

        if issubclass(cls, Registry):
            assert cls.is_registry
            assert not cls.is_plugin
        elif issubclass(cls, Plugin):
            assert not cls.is_registry
            assert cls.is_plugin

        if cls.is_plugin and cls._registry is None:
            assert cls.entry_point is None
        else:
            assert isinstance(cls.entry_point, pkg_resources.EntryPoint)

    def test_plugin_tree(self):
        tree = sorted([_[1] for _ in path_tests_string if _[0] != Plugin])
        assert tree == Registry.tree

    def test_entry_points(self):
        eps = Registry.entry_points()
        for ep in eps:
            assert re.match(pkg_resources.EntryPoint.pattern.pattern, ep)
        eps = Registry.entry_points(align=False)
        for ep in eps:
            assert re.match(pkg_resources.EntryPoint.pattern.pattern, ep)
